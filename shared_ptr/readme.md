# shared_ptr

https://github.com/llvm-mirror/libcxx/blob/master/include/memory

`__libcpp_atomic_refcount_decrement`

```cpp
return __atomic_add_fetch(&__t, -1, __ATOMIC_ACQ_REL);
```

`__libcpp_atomic_refcount_increment`:
```cpp
return __atomic_add_fetch(&__t, 1, __ATOMIC_RELAXED);
```

https://stackoverflow.com/questions/14881056/confusion-about-implementation-error-within-shared-ptr-destructor/28422424#28422424

---

Упорядочиваем `ptr->Mutate()` и вызов деструктора `T`.

---

https://eel.is/c++draft/intro.multithread

>  A release sequence headed by a release operation A on an atomic object M is a maximal contiguous sub-sequence of side effects in the modification order of M, where the first operation is A, and every subsequent operation is an atomic read-modify-write operation.


https://eel.is/c++draft/atomics.order

> An atomic operation A that performs a release operation on an atomic object M synchronizes with an atomic operation B that performs an acquire operation on M and takes its value from any side effect in the release sequence headed by A.

`W_rel -> RMW_rlx -> RMW_rlx -> R_acq` => `W_rel` _synchronizes-with_ `R_acq` 

`W_rel -> RMW_rlx -> RMW_rlx` – _release sequence_

---

https://stackoverflow.com/questions/49112732/memory-order-in-shared-pointer-destructor