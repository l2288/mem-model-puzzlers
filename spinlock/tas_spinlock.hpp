#pragma once

#include <atomic>

// Test-and-Set spinlock

class SpinLock {
 public:
  void Lock() {
    while (locked_.exchange(true /*, mo? */) {
      Pause();
    }
  }

  void Unlock() {
    locked_.store(false /*, mo? */);
  }

 private:
  static void Pause() {
    asm volatile("pause\n" : : : "memory");
  }

 private:
  std::atomic<bool> locked_{false};
}
