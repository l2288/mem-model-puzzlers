#include <atomic>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

////////////////////////////////////////////////////////////

class ForwardList {
  struct Node {
    Node* next;
  };

 public:
  void Push() {
    Node* new_node = new Node{head_};
    head_ = new_node;
  }

  void Pop() {
    Node* head = head_;
    head_ = head->next;
    delete head;
  }

 private:
  Node* head_{nullptr};
};

////////////////////////////////////////////////////////////

class SpinLock {
 public:
  void lock() {  // NOLINT
    while (locked_.exchange(true)) {
      ;
    }
  }

  void unlock() {  // NOLINT
    locked_.store(false);
  }

 private:
  std::atomic<bool> locked_{false};
};

////////////////////////////////////////////////////////////

int main() {
  ForwardList list;  // Guarded by spinlock
  SpinLock spinlock;

  // Just works

  list.Push();
  list.Push();
  list.Pop();
  list.Pop();
  list.Push();
  list.Pop();

  // Concurrent access

  std::vector<std::thread> threads;

  for (size_t i = 0; i < 5; ++i) {
    threads.emplace_back([&]() {
      for (size_t k = 0; k < 100500; ++k) {
        //std::lock_guard guard(spinlock);
        list.Push();
        list.Pop();
      }
    });
  }

  for (auto& t : threads) {
    t.join();
  }

  return 0;
}

