# Memory Model Puzzlers

## Puzzlers

### Basic

- [Spinlock](spinlock/tas_spinlock.hpp)
- [SP/SC Ring Buffer](ring-buffer/ring_buffer.hpp)
- [LazyValue](lazy-value/lazy_value.hpp)

## Examples

- [Store Buffering](store_buffering/main.cpp)
- [Store Buffering / Atomics](store_buffering_atomics/main.cpp)
- [Godbolt: Memory orders, Compiler reordering](godbolt)
