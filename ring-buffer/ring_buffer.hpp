#pragma once

#include <atomic>
#include <vector>

// Single-Producer/Single-Consumer Fixed-Size Ring Buffer (Queue)

template <typename T>
class SPSCRingBuffer {
 public:
  explicit SPSCRingBuffer(const size_t capacity) : buffer_(capacity + 1) {
  }

  bool Publish(T value) {
    const size_t curr_head = head_.load(/* memory order? */);
    const size_t curr_tail = tail_.load(/* memory_order? */);

    if (IsFull(curr_head, curr_tail)) {
      return false;
    }

    buffer_[curr_tail] = std::move(value);
    tail_.store(Next(curr_tail) /*, memory order */);
    return true;
  }

  bool Consume(T& value) {
    const size_t curr_head = head_.load(/* memory order? */);
    const size_t curr_tail = tail_.load(/* memory order? */);

    if (IsEmpty(curr_head, curr_tail)) {
      return false;
    }

    value = std::move(buffer_[curr_head]);
    head_.store(Next(curr_head) /*, memory order? */);
    return true;
  }

 private:
  bool IsFull(const size_t head, const size_t tail) const {
    return Next(tail) == head;
  }

  bool IsEmpty(const size_t head, const size_t tail) const {
    return tail == head;
  }

  size_t Next(const size_t slot) const {
    return (slot + 1) % buffer_.size();
  }

 private:
  std::vector<T> buffer_;
  std::atomic<size_t> tail_{0};
  std::atomic<size_t> head_{0};
};
